<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Plugin\Field\FieldType\XtTarActFieldType.
 */

namespace Drupal\sxt_xttaract\Plugin\Field\FieldType;

//use Drupal\sxt_xttaract\SlogXtTarAct;
use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\sloggen\SlogGen;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;

/**
 * @FieldType(
 *   id = "xttaract_fieldtype",
 *   label = @Translation("XtTargetActual"),
 *   description = @Translation(".................XtTargetActual Field Type."),
 *   default_formatter = "xttaract_formatter",
 *   default_widget = "xttaract_widget"
 * )
 */
class XtTarActFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'content';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['item_id'] = DataDefinition::create('integer')
            ->setLabel(t('Instance ID'))
            ->setSetting('unsigned', TRUE)
            ->setInternal(TRUE)
            ->setRequired(FALSE);

    $properties['content'] = DataDefinition::create('string')
            ->setLabel(t('Content'))
            ->setRequired(FALSE);

    $properties['target'] = DataDefinition::create('string')
            ->setLabel(t('Target'))
            ->setRequired(FALSE);

    $properties['actual'] = DataDefinition::create('string')
            ->setLabel(t('Actual'))
            ->setRequired(FALSE);

    $properties['comment'] = DataDefinition::create('string')
            ->setLabel(t('Comment'))
            ->setRequired(FALSE);

    $properties['uid'] = DataDefinition::create('integer')
            ->setLabel(t('Owner'))
            ->setSetting('unsigned', TRUE);

    $properties['region'] = DataDefinition::create('integer')
            ->setLabel(t('State'))
            ->setSetting('unsigned', TRUE);

    $properties['weight'] = DataDefinition::create('integer')
            ->setLabel(t('Weight'))
            ->setSetting('unsigned', FALSE);

    $properties['xtra'] = DataDefinition::create('string')
            ->setLabel(t('More instance related serialized data'))
            ->setInternal(TRUE)
            ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
        'columns' => [
            'item_id' => [
                'type' => 'int',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
            ],
            'content' => [
                'type' => 'varchar',
                'length' => 255,
            ],
            'target' => [
                'type' => 'varchar',
                'length' => 255,
            ],
            'actual' => [
                'type' => 'varchar',
                'length' => 255,
            ],
            'comment' => [
                'type' => 'varchar',
                'length' => 255,
            ],
            'uid' => [
                'type' => 'int',
                'unsigned' => TRUE,
            ],
            'region' => [
                'type' => 'int',
                'size' => 'tiny',
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'weight' => [
                'type' => 'int',
                'unsigned' => FALSE,
                'default' => 0,
            ],
            'xtra' => [
                'type' => 'blob',
                'size' => 'normal',
                'serialize' => TRUE,
                'not null' => FALSE,
            ],
        ],
        'indexes' => [
            'content' => ['content'],
            'region' => ['region'],
        ],
        'unique keys' => [
            'item_id' => ['item_id'],
        ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = SlogGen::getRandom();
    $user_ids = SlogGen::getRandomUserIds(1);

    $parts = explode('.', $random->sentences(mt_rand(3, 10)), 2);
    $content  = $parts[0];
    $more =  !empty($parts[1]) ? trim($parts[1]) : false;
    if (!empty($more)) {
      $content .= "\n\n$more";
    }
    
    $value = [
        'content' => $content,
        'target' => $random->sentences(mt_rand(4, 20)),
        'actual' => $random->sentences(mt_rand(2, 5)),
        'comment' => $random->sentences(mt_rand(2, 10)),
        'region' => mt_rand(SlogXtItems::XTXSI_IDX_UNRATED, SlogXtItems::XTXSI_IDX_BEST),
        'uid' => $user_ids[0],
        'weight' => mt_rand(0, 500),
    ];

    return $value;
  }

}
