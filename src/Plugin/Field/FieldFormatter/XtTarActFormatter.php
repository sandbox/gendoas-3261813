<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Plugin\Field\FieldFormatter\XtTarActFormatter.
 */

namespace Drupal\sxt_xttaract\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\sxt_xtitems\Plugin\Field\FieldFormatter\XtItemsFormatter;
use Drupal\sxt_xttaract\Form\XtTarActTrait;

/**
 * Plugin implementation of the XtTarAct formatter.
 *
 * @FieldFormatter(
 *   id = "xttaract_formatter",
 *   label = @Translation("XtTargetActual"),
 *   description = @Translation("XtTarAct Formatter"),
 *   field_types = {
 *     "xttaract_fieldtype"
 *   }
 * )
 */
class XtTarActFormatter extends XtItemsFormatter {

 use XtTarActTrait;

  protected function contentBuild(array $info, $langcode) {
    $delta = $info['delta'];
    $itemid = $info['item_id'];

    $container = [
        '#type' => 'container',
        '#attributes' => [
            'class' => 'xtitems-item xttaract-item',
        ],
    ];

    // content
    $content = (string) $info['content'];
    $container['content'] = [
        '#type' => 'processed_text',
        '#text' => $this->prepareXtsiMoreLink($content, $delta, $itemid),
        '#format' => $this->xtitemsFormat(),
        '#langcode' => $langcode,
        '#prefix' => '<div class="xttaract-content">',
        '#suffix' => '</div>',
    ];

    // table with target and actual
    $target = (string) $info['target'];
    $actual = (string) $info['actual'];
    $container['taract'] = [
        '#type' => 'table',
        '#header' => [
            'target' => t('Target'),
            'actual' => t('Actual')
        ],
        '#prefix' => '<div class="xttaract-table">',
        '#suffix' => '</div>',
        '#rows' => [
          'single' => [
            'target' => $this->renderedContent($target, $delta, $itemid, $langcode),
            'actual' => $this->renderedContent($actual, $delta, $itemid, $langcode),
          ],
        ],
    ];

    // comment
    $comment = (string) $info['comment'];
    $container['comment'] = [
        '#type' => 'processed_text',
        '#text' => $this->prepareXtsiMoreLink($comment, $delta, $itemid),
        '#format' => $this->xtitemsFormat(),
        '#langcode' => $langcode,
        '#prefix' => '<div class="xttaract-comment">',
        '#suffix' => '</div>',
    ];

    return $container;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $elements['#attached']['library'][] = 'sxt_xttaract/sxt_xttaract.core';
    return $elements;
  }

}
