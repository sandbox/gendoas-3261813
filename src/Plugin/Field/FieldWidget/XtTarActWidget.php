<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Plugin\Field\FieldType\XtTarActWidget.
 */

namespace Drupal\sxt_xttaract\Plugin\Field\FieldWidget;

use Drupal\sxt_xtitems\Plugin\Field\FieldWidget\XtItemsWidget;
use Drupal\sxt_xttaract\Form\XtTarActTrait;

/**
 * @FieldWidget(
 *   id = "xttaract_widget",
 *   label = @Translation("XtTargetActual"),
 *   description = @Translation(".............XtTarAct Widget"),
 *   field_types = {
 *     "xttaract_fieldtype"
 *   },
 *   multiple_values = TRUE
 * )
 */
class XtTarActWidget extends XtItemsWidget {

  use XtTarActTrait;

  /**
   */
  protected function getRouteName($for_xt_dialog, $action) {
    $prefix = $for_xt_dialog ? 'sxt_xttaract.ajax.xtitems' : 'sxt_xttaract.xtitems';
    return "$prefix.$action";
  }

  /**
   */
  protected function onMassageFormValues(array &$new_values, array $item_values) {
    $item_id = $item_values['item_id'];
    $info =  $this->savedInfo[$item_id];

    $new_values['target'] = $info['target'];
    $new_values['actual'] = $info['actual'];
    $new_values['comment'] = $info['comment'];
  }

}
