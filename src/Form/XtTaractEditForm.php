<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Form\XtTaractEditForm.
 */

namespace Drupal\sxt_xttaract\Form;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_xtitems\Form\XtItemEditForm;
use Drupal\slogxt\XtExtrasTrait;

/**
 */
class XtTaractEditForm extends XtItemEditForm {

  use XtTarActTrait;
  use XtExtrasTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_xttaract_xtitem_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $for_xticollect = (boolean) $request->get('for_xticollect');
    $xtitem_id = (integer) $request->get('xtitem_id');
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);
    $this->delta = $this->getDeltaFromItemId($xtitems, $xtitem_id);
    $xtitem = $xtitems->get($this->delta);
    if ($xtitem) {
      $values = $xtitem->getValue();

      $r_num = $this->getNumDoneRatings($values['xtra']);
      if ($r_num > 0) {
        $args = ['%num' => $r_num];
        $msg = t('WARNING: Updating will remove already done single ratings (number=%num).', $args);
        $form['message'] = [
            '#type' => 'markup',
            '#markup' => SlogXt::htmlMessage($msg, 'warning'),
        ];
      }

      $this->old_content = [
          'content' => $values['content'],
          'target' => $values['target'],
          'actual' => $values['actual'],
          'comment' => $values['comment'],
      ];
      
      $c_title = $for_xticollect ? t('Text') : t('Abstract');
      $this->addFieldXtContent($form, 'content', $c_title, $values['content']);
      if (!$for_xticollect) {
        $this->addFieldXtContent($form, 'target', t('Target'), $values['target']);
        $this->addFieldXtContent($form, 'actual', t('Actual'), $values['actual']);
        $this->addFieldXtContent($form, 'comment', t('Comment'), $values['comment']);
      }
      $this->addFieldActionSubmit($form, t('Update'));
    } else {
      $args = [
          '%nid' => $this->node->id(),
          '%itemid' => $xtitem_id,
      ];
      $msg = t('Item not found (nid=%nid, iid=%itemid)', $args);
      $form['message'] = [
          '#type' => 'markup',
          '#markup' => SlogXt::htmlMessage($msg, 'error'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $has_change = FALSE;
    foreach ($this->getContentFields() as $key) {
      if (($values[$key] ?? '') !== $this->old_content[$key]) {
        $has_change = TRUE;
        break;
      }
    }
    if (!$has_change) {
      $form_state->setErrorByName('content', t('No changes have been done.'));
      return;
    }
    
    $input = (string) $values['content'];
    $input_array = $this->splitInput($input);
    if (empty($input_array)) {
      $form_state->setErrorByName('content', t('Text field is required.'));
    } elseif (count($input_array) > 1) {
      $args = ['%sep' => $this->getSeparator()];
      $form_state->setErrorByName('content', t('Multiple items with leading "%sep" is not allowed.', $args));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $field_name = $this->getFieldName();
    $xtitems = $this->node->get($field_name);
    $xtitem = $xtitems->get($this->delta);
    $new_values = $xtitem->getValue();
    foreach ($this->getContentFields() as $key) {
      $new_values[$key] = (string) ($values[$key] ?? '');
    }
    
    if (!empty($new_values['xtra'])) {
      $new_values['xtra'] = $this->unsetRatedState($new_values['xtra']);
    }

    $xtitems->set($this->delta, $new_values);
    $this->node->save();
    
    \Drupal::messenger()->addStatus(t('Item has been updated'));
  }

}
