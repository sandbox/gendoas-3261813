<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Form\XtTaractDeleteConfirmForm.
 */

namespace Drupal\sxt_xttaract\Form;

use Drupal\sxt_xtitems\Form\XtItemDeleteConfirmForm;

/**
 */
class XtTaractDeleteConfirmForm extends XtItemDeleteConfirmForm {

  use XtTarActTrait;
}
