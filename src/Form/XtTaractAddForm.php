<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Form\XtTaractAddForm.
 */

namespace Drupal\sxt_xttaract\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtExtrasTrait;
use Drupal\sxt_xtitems\Form\XtItemAddForm;

/**
 */
class XtTaractAddForm extends XtItemAddForm {

  use XtTarActTrait;
  use XtExtrasTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_xttaract_xtitem_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $for_xticollect = (boolean) $request->get('for_xticollect');
    $this->for_region = $region = (integer) $request->get('region');

    $c_title = $for_xticollect ? t('Text') : t('Abstract');
    $this->addFieldXtContent($form, 'content', $c_title, NULL, $region);
    if (!$for_xticollect) {
      $this->addFieldXtContent($form, 'target', t('Target'));
      $this->addFieldXtContent($form, 'actual', t('Actual'));
      $this->addFieldXtContent($form, 'comment', t('Comment'));
      $this->addFieldPrepend($form);
    }
    $this->addFieldActionSubmit($form, t('Add new'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = (integer) \Drupal::currentUser()->id();
    $values = $form_state->getValues();
    $prepend = (boolean) $values['prepend'];
    $field_name = $this->getFieldName();
    $xttaract = $this->node->get($field_name);

    $input_values = [];
    foreach ($this->getContentFields() as $key) {
      $input_values[$key] = $this->splitInput((string) ($values[$key] ?? ''));
    }
    $content_array = $input_values['content'];
    $input_count = count($content_array);

    $weight = 0;
    $minmax = $this->getMinMaxForRegion($xttaract, $this->for_region);
    extract($minmax);
    if (!$is_empty) {
      $weight = $prepend ? $min - $input_count : $max + 1;
    }

    foreach ($content_array as $idx => $input) {
      $new_value = [
          'content' => $input,
          'target' => $input_values['target'][$idx] ?? '',
          'actual' => $input_values['actual'][$idx] ?? '',
          'comment' => $input_values['comment'][$idx] ?? '',
          'region' => $this->for_region,
          'uid' => $uid,
          'weight' => $weight,
      ];
      $xttaract->appendItem($new_value);
      $weight++;
    }

    $this->node->save();

    $msg = ($input_count > 1) ? t('Items have been added.') : t('Item has been added.');
    \Drupal::messenger()->addStatus($msg);
  }

}
