<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Form\XtTarActTrait.
 */

namespace Drupal\sxt_xttaract\Form;

use Drupal\sxt_xttaract\SlogXtTarAct;
use Drupal\sxt_xtitems\SlogXtItems;
use Drupal\sxt_xtitems\Form\XtItemTrait;
use Drupal\slogxt\XtExtrasTrait;

/**
 */
trait XtTarActTrait {

  use XtItemTrait;
  use XtExtrasTrait;

  protected function getBundle() {
    return 'xttaract';
  }

  protected function getFieldName() {
    return 'xttaract';
  }

  protected function getContentFields() {
    return ['content', 'target', 'actual', 'comment'];
  }

  protected function getRegionLabelsFromRequest() {
    return \Drupal::request()->get('xttaract_regions');
  }

  protected function getRatingOptions($off = 1) {
    return [
        SlogXtItems::XTXSI_IDX_UNRATED + $off => '__' . (string) t('unrated') . '__',
        SlogXtItems::XTXSI_IDX_BAD + $off => (string) t('Rejected (unsuitable for this listing)'),
        SlogXtItems::XTXSI_IDX_VALID + $off => (string) t('Resubmitted'),
        SlogXtItems::XTXSI_IDX_FINE + $off => (string) t('Bookmarked'),
        SlogXtItems::XTXSI_IDX_BEST + $off => (string) t('Adopted'),
    ];
  }

  protected function renderedContent($content, $delta, $itemid, $langcode) {
    $element = [
        '#type' => 'processed_text',
        '#text' => $this->prepareXtsiMoreLink($content, $delta, $itemid),
        '#format' => $this->xtitemsFormat(),
        '#langcode' => $langcode,
    ];
    return \Drupal::service('renderer')->render($element);
  }

  protected function prepareInfoContents(array $values) {
    $contents = [
        'content' => $values['content'],
        'target' => $values['target'] ?? '',
        'actual' => $values['actual'] ?? '',
        'comment' => $values['comment'] ?? '',
    ];
    return $contents;
  }

}
