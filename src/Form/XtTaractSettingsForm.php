<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Form\XtTaractSettingsForm.
 */

namespace Drupal\sxt_xttaract\Form;

use Drupal\slogxt\XtExtrasTrait;
use Drupal\sxt_xtitems\Form\XtItemSettingsForm;

/**
 */
class XtTaractSettingsForm extends XtItemSettingsForm {

  use XtTarActTrait;
  use XtExtrasTrait;

  /**
   */
  public function getFormId() {
    return 'sxt_xttaract_xtitem_settings';
  }

  /**
   */
  public function isTaract() {
    return TRUE;
  }

}
