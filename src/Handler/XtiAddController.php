<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Handler\XtiAddController
 */

namespace Drupal\sxt_xttaract\Handler;

use Drupal\sxt_xtitems\Handler\XtItemControllerBase;
use Drupal\sxt_xttaract\Form\XtTarActTrait;

/**
 */
class XtiAddController extends XtItemControllerBase {

  use XtTarActTrait;

  protected $region_name;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $region = $request->get('region');
    $region_labels = $this->getRegionLabelsByNode($this->node);
    $this->region_name = $region_labels[$region] ?? '???';

    return 'Drupal\sxt_xttaract\Form\XtTaractAddForm';
  }

  protected function getFormTitle() {
    return t('Add new for region %region', ['%region' => $this->region_name]);
  }

  protected function getSubmitLabel() {
    return t('Add new');
  }

  protected function getDoneMessage() {
    return t('New item(s) have been added.');
  }
  
}
