<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Handler\XtiEditController
 */

namespace Drupal\sxt_xttaract\Handler;

use Drupal\sxt_xtitems\Handler\XtItemControllerBase;

/**
 */
class XtiEditController extends XtItemControllerBase {

  protected $node;
  
  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->node = \Drupal::request()->get('node');
    
    return 'Drupal\sxt_xttaract\Form\XtTaractEditForm';
  }

  protected function getFormTitle() {
    return t('Edit item');
  }

  protected function getSubmitLabel() {
    return t('Update');
  }

  protected function getDoneMessage() {
    return t('Item has been updated.');
  }
  

}
