<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\Handler\XtiSettingsController
 */

namespace Drupal\sxt_xttaract\Handler;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\sxt_xtitems\Handler\XtItemControllerBase;
use Drupal\sxt_xttaract\Form\XtTarActTrait;

/**
 */
class XtiSettingsController extends XtItemControllerBase {

  use XtTarActTrait;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    if (!XtsiNodeTypeData::isXtNode($this->node)) {
      return SlogXt::arrangeUrgentMessageForm(t('Node is not Xt-Node-Type.'), 'error');
    }

    return 'Drupal\sxt_xttaract\Form\XtTaractSettingsForm';
  }

  protected function getFormTitle() {
    return t('Settings');
  }

  protected function getSubmitLabel() {
    return t('Save settings');
  }

  protected function getDoneMessage() {
    return t('Settings have been saved.');
  }
  
}
