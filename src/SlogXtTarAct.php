<?php

/**
 * @file
 * Contains \Drupal\sxt_xttaract\SlogXtTarAct.
 */

namespace Drupal\sxt_xttaract;

use Drupal\sxt_xtitems\SlogXtItems;

/**
 */
class SlogXtTarAct extends SlogXtItems {

  public static function getRegionLabelsDefault() {
    $regions = [
        SlogXtItems::XTXSI_IDX_UNRATED => (string) t('Unrated'),
        SlogXtItems::XTXSI_IDX_BAD => (string) t('Rejected'),
        SlogXtItems::XTXSI_IDX_VALID => (string) t('Resubmitted'),
        SlogXtItems::XTXSI_IDX_FINE => (string) t('Bookmarked'),
        SlogXtItems::XTXSI_IDX_BEST => (string) t('Adopted'),
    ];

    return array_reverse($regions, TRUE);
  }

}
